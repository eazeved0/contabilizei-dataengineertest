# Contabilizei - Data Engineer Technical Test

## Coding Test

Ol�! Primeiramente: Valeu pelo seu tempo para realizar esse teste t�cnico!

Ele � bem simples e dividido em 2 principais objetivos:

1 - Connect and Crawl
2 - Transform and Keep

A sua tarefa � escrever um app que consuma uma API (https://api-sandbox.contabilizei.com/ds/) e que seja capaz de:

 - Realizar pesquisas
 - Transformar o resultado obtido de json para csv
 - Fazer a persist�ncia do resultado das pesquisas e do hist�rico das ultimas 3 pesquisas
 
 Para isso, disponibilizamos em https://api-sandbox.contabilizei.com/ds/customers dados fict�cios de algumas empresas na seguinte estrutura:
 
 ```
 [
  {
    "company_Id": "",
    "owner_name": "",
    "owner_surname": "",
    "owner_gender": "",
    "owner_age": "",
    "company_state": [],
    "Products_list": [
      {
        "product": [
          ""
        ],
        "product_price": ""
      },
    ],
    "comments": ""
  }
]
```

Como exemplo, uma chamada para https://api-sandbox.contabilizei.com/ds/customers?company_Id=cfcd208495d565ef retornar� os dados apenas de uma empresa.

Seu app deve aceitar 2 par�metros de busca: product e company_state.

Seu c�digo deve ser disponibilizado via reposit�rio e n�s teremos que ser capazes de rodar seu c�digo apenas seguindo instru��es do Readme, ok?


## Platform Choice

Voc� est� livre para escolher qualquer tecnologia, desde que a entrega atenda os objetivos do teste.


## Task Requirements

- Seu c�digo deve executar em apenas 1 step
- Voc� **deve** incluir testes
- Evite incluir artefatos de seu build local (pasta "bin" por exemplo)
- Sua �ltima busca precisa ser armazenada (hist�rico de busca), al�m disso, o resultado da busca dever� ser exportado em formato csv.


## User Story e Crit�rio de Aceita��o

* Como usu�rio farei duas buscas:

1) Todas as empresas em dois estados distintos (ex: SP e PR) e que comercializem "ketchup"

2) Todas as empresas que comercializem "water"

* Eu recebo o resultado na tela no seguinte formato para cada uma das buscas:

 - Quantidade total de empresas encontradas (ex: 14 empresas encontradas)
 - Descri��o  de todas as empresas localizadas no formato "product" | "company_Id" | "product_price" |  "company_state" (ex: water | cfcd208495d565ef | 10.8997 | SP, PR)
 - Path do csv com o resultado das buscas realizadas no formato "date[ddmmaaaa] + company_Id + qtd_results.csv" (ex: /tmp/02-01-2019-cfcd208495d565ef_14.csv)
 - Busca realizada (ex: Resultado da busca por "water" em "SP e PR")


## Entrega
 
  A URL do reposit�rio com o c�digo para ser avaliado, considerando as premissas j� descritas.
  
  
 Boa sorte!!



